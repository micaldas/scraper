from setuptools import setup

setup(
    name="scraper",
    version=2.0,
    author="mclds",
    author_email="mclds@protonmail.com",
    description="Automates the scraping of a site.",
    long_description="README.md",
    long_description_content_type="text/markdown",
    url="https://codeberg.org/micaldas/scraper",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    packages=["scraper"],
    install_requires=[
        "scrapy",
        "scraperr",
        "loguru",
    ],
    include_package_data=True,
)
